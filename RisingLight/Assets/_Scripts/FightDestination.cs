﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace LAPMTech
{
    // this script is for the spawns and destinations of the soldiers
    public class FightDestination : MonoBehaviour
    {
        // scripts of every soldier on the field
        private PlayerControl[] playerControll;
        private EnemyControl[] enemyControll;

        // every enemy and player soldier on the field
        [SerializeField]
        private GameObject[] player;
        [SerializeField]
        private GameObject[] enemy;

        // the destination of every player soldier
        [SerializeField]
        private Transform[] playerDestination;

        // the destination of every enemy soldier
        [SerializeField]
        private Transform[] enemyDestination;

        // the enemy spawn point of every soldier
        [SerializeField]
        private Transform[] enemyStartPoint;

        // the player spawn point of every soldier
        [SerializeField]
        private Transform[] playerStartPoint;

        // how fast the soldiers get to their destination
        [SerializeField]
        private float maxDistanceDelta;

        // Prefab of the enemy and player soldier to spawn the new ones with Instantiate
        [SerializeField]
        private GameObject playerSoldier;
        [SerializeField]
        private GameObject enemySoldier;

        // display the enemy and the player kills
        [SerializeField]
        private TextMeshProUGUI numberOfEnemies;
        [SerializeField]
        private TextMeshProUGUI numberOfPlayer;

        // counter for the player kills
        private int playerKills;
        public int PlayerKills
        {
            get
            {
                return playerKills;
            }
            set
            {
                playerKills = value;
            }
        }

        // counter for the enemy kills
        private int enemyKills;
        public int EnemyKills
        {
            get
            {
                return enemyKills;
            }
            set
            {
                enemyKills = value;
            }
        }

        void Start()
        {
            // get the scripts of every soldier
            enemyControll = FindObjectsOfType<EnemyControl>();
            playerControll = FindObjectsOfType<PlayerControl>();

            // get the animator of every soldier
            for (int i = 0; i <= 6; i++)
            {
                player[i].GetComponent<Animator>().SetBool("RUN", true);
                enemy[i].GetComponent<Animator>().SetBool("RUN", true);
            }
        }

        void Update()
        {
            // the method is in Update, because of the if condition and the soldiers, which will move after the spawn
            MoveToFixedPoint();

            // updates the count of the kills every second 
            // convert the soldier kill numbers in text to display it
            numberOfPlayer.text = string.Format("Your Kills: " + playerKills);
            numberOfEnemies.text = string.Format("Enemy Kills: " + enemyKills);

        }

        // move the soldier to a fixed point, after the respawn/spawn
        // if an enemy or a player soldier dies, he will be respawned new and the counter will be counted plus 1 
        // the IDLE animation will be played if the soldier has no enemy and is at his destination
        // the RUN animation will be played if the soldier wants to reach his destination
        // the FIGHT animation will be played if the soldier is at the destination and is in front of his enemy
        void MoveToFixedPoint(int counter = 6)
        {
            for (int i = 0; i <= counter; i++)
            {
                if (player[i] != null && enemy[i] != null)
                {
                    if (playerControll[i].PlayerHealth <= 0 || enemyControll[i].EnemyHealth <= 0)
                    {
                        player[i].GetComponent<Animator>().SetBool("FIGHT", false);
                        enemy[i].GetComponent<Animator>().SetBool("FIGHT", false);
                    }

                    Vector3 movePlayer = Vector3.MoveTowards(player[i].transform.position, playerDestination[i].position, maxDistanceDelta * Time.deltaTime);
                    Vector3 moveEnemy = Vector3.MoveTowards(enemy[i].transform.position, enemyDestination[i].position, maxDistanceDelta * Time.deltaTime);
                    player[i].transform.position = movePlayer;
                    enemy[i].transform.position = moveEnemy;
                }

                if (player[i] == null)
                {
                    GameObject playerTeam = Instantiate(playerSoldier, playerStartPoint[i].position, Quaternion.identity);
                    playerTeam.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
                    playerTeam.transform.position = playerStartPoint[i].position;
                    player[i] = playerTeam;

                    player[i].GetComponent<Animator>().SetBool("RUN", true);

                    enemyKills++;
                }

                if (enemy[i] == null)
                {
                    GameObject enemyTeam = Instantiate(enemySoldier, enemyStartPoint[i].position, Quaternion.identity);
                    enemyTeam.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
                    enemyTeam.transform.position = enemyStartPoint[i].position;
                    enemyTeam.transform.Rotate(0, 180, 0);
                    enemy[i] = enemyTeam;

                    enemy[i].GetComponent<Animator>().SetBool("RUN", true);

                    playerKills++;
                }

                if (player[i].transform.position == playerDestination[i].position)
                {
                    player[i].GetComponent<Animator>().SetBool("RUN", false);
                }

                if (enemy[i].transform.position == enemyDestination[i].position)
                {
                    enemy[i].GetComponent<Animator>().SetBool("RUN", false);
                }

                if (enemy[i].transform.position == enemyDestination[i].position && player[i].transform.position == playerDestination[i].position)
                {
                    player[i].GetComponent<Animator>().SetBool("FIGHT", true);
                    enemy[i].GetComponent<Animator>().SetBool("FIGHT", true);
                }
            }
        }
    }
}
