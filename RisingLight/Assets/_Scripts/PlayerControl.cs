﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LAPMTech
{
    //Every soldier gets this script
    public class PlayerControl : MonoBehaviour
    {

        [SerializeField]
        private Slider HealthBarPlayer;

        private int playerHealth = 500;
        public int PlayerHealth
        {
            get
            {
                return playerHealth;
            }
            set
            {
                playerHealth = value;
            }
        }

        private int enemyDamage = 80;

        void Update()
        {
            // if the player health reached 0 the soldier will be destroyed
            if (playerHealth <= 0)
            {
                Destroy(gameObject);
            }

            // the healthbar will be updated every time to get the current health
            HealthBarPlayer.value = playerHealth;
        }

        // if the sword of the enemy hits the player, he will get damage
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("EnemyTeam"))
            {
                playerHealth -= enemyDamage;

            }
        }

    }
}
