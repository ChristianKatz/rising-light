﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LAPMTech
{
    public class MainMenu : MonoBehaviour
    {
        // the name of the scene, that is the battleground
        private string SceneName = "Play";

        private void Start()
        {
            // making sure that the game will running
            Time.timeScale = 1;
        }

        //close the Game
        public void EndGame()
        {
            Application.Quit();
        }

        // Load the "Play" Scene, that is the battleground
        public void StartGame()
        {
            SceneManager.LoadScene(SceneName);
        }
    }
}
