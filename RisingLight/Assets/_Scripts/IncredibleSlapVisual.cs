﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
    // this script is for the activation of the visual effect of the spell "Incredible Slap"
    public class IncredibleSlapVisual : MonoBehaviour
    {
        // the particle system, that shows up, when the spell "Incredible Slap" is activated
        [SerializeField]
        private GameObject playerMagicBoost;

        // script that shows, if the spell is activated
        private Skilltree skilltree;

        void Start()
        {
            // get the scripts
            skilltree = FindObjectOfType<Skilltree>();
        }

        void Update()
        {
            // If the spell is activated the visual effect shows up
            if (skilltree.VisualIncredibleSlapIsActivated == true)
                playerMagicBoost.SetActive(true);

            // If the spell is deactivated the visual effect doesn't show up
            if (skilltree.VisualIncredibleSlapIsActivated == false)
                playerMagicBoost.SetActive(false);

        }
    }
}
