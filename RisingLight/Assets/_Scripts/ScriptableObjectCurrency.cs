﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
    // this ScriptableObject is for the Currency to get passive income
    [CreateAssetMenu(fileName = "Wage Update", menuName = "ScriptableObjects/ Wage Update", order = 2)]
    public class ScriptableObjectCurrency : ScriptableObject
    {
        public int cost;
        public int wage;
        public float wageCooldownInSeconds;

    }
}
