﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
    // this ScriptableObject is for the 3 damage Upgrades of the sword
    [CreateAssetMenu(fileName = "Sword Update", menuName = "ScriptableObjects/ Sword Update", order = 1)]
    public class ScriptableObjectSwordUpgrades : ScriptableObject
    {
        public int cost;
        public int damage;
        public float damageCooldownInSecond;

    }
}
