﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LAPMTech
{
    //Every soldier gets this script
    public class EnemyControl : MonoBehaviour
    {

        [SerializeField]
        private Slider HealthBar;

        private int enemyHealth = 500;
        public int EnemyHealth
        {
            get
            {
                return enemyHealth;
            }
            set
            {
                enemyHealth = value;
            }
        }

        private int playerDamage = 10;
        public int PlayerDamage
        {
            get
            {
                return playerDamage;
            }
            set
            {
                playerDamage = value;
            }
        }

        // the damage of the "IncredibleSlap" spell
        private int incredibleSlapDamage = 0;
        public int IncredibleSlapDamage
        {
            get
            {
                return incredibleSlapDamage;
            }
            set
            {
                incredibleSlapDamage = value;
            }
        }

        // get the ScriptableObjects
        public ScriptableObjectMagicAbilities[] MagicAbility;

        void Update()
        {
            //the healtbar will be updated every time to get the current health
            HealthBar.value = enemyHealth;

            // if the enemy health reached 0, the soldier will be destroyed
            if (enemyHealth <= 0)
            {
                Destroy(gameObject);
            }
        }

        // the enemy loses health, if the spells or the sword of the player hits them
        private void OnTriggerEnter(Collider other)
        {
            for (int i = 0; i <= 2; i++)
            {
                if (other.CompareTag(MagicAbility[i].triggerTag))
                {
                    enemyHealth -= MagicAbility[i].magicDamage;
                }
            }

            if (other.CompareTag("PlayerTeam"))
            {
                enemyHealth -= playerDamage;
                enemyHealth -= IncredibleSlapDamage;
            }

        }
    }
}
